import { Component, OnInit } from "@angular/core";
import { ActivatedRoute,Router } from "@angular/router";
import { UserServiceService } from "src/app/Services/user-service.service";

@Component({
  selector: "app-post-details",
  templateUrl: "./post-details.component.html",
  styleUrls: ["./post-details.component.scss"],
})
export class PostDetailsComponent implements OnInit {
  post: any;
  comments: any[] = [];
  constructor(
    private ActivatedRoute: ActivatedRoute,
    private route:Router,
    private userServ: UserServiceService
  ) {}

  ngOnInit() {
    this.ActivatedRoute.params.subscribe((res: any) => {
      console.log("res of route: ", res);
      if (res.id) {
        this.getPostdetails(res.id);
        this.getComments(res.id);
      }
    });
  }

  getPostdetails(id) {
    this.userServ.getPostDetail(id).subscribe(
      (res: any) => {
        this.post = res;
      },
      (err) => {
        console.log("error of getDetail: ", err);
      }
    );
  }

  getComments(postId) {
    this.userServ.getPostComments(postId).subscribe(
      (res: any) => {
        this.comments = res;
      },
      (err) => {
        console.log("error of getDetail: ", err);
      }
    );
  }

  toEdit(){
    this.route.navigate([`/post-edit/${this.post.id}`]);
  }
}
