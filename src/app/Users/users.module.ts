import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostDetailsComponent } from './post-details/post-details.component';
import { UserPostsComponent } from './user-posts/user-posts.component';
import { EditPostComponent } from './edit-post/edit-post.component';



@NgModule({
  declarations: [PostDetailsComponent, UserPostsComponent, EditPostComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class UsersModule { }
