import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EditPostComponent } from "./edit-post/edit-post.component";
import { PostDetailsComponent } from "./post-details/post-details.component";
import { UserPostsComponent } from "./user-posts/user-posts.component";

const routes: Routes = [
  //   { path: "", component: AddProductComponent },
  { path: "", component: UserPostsComponent },
  { path: "post/:id", component: PostDetailsComponent },
  { path: "post-edit/:id", component: EditPostComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
