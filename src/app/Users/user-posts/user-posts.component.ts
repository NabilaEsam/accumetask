import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserServiceService } from "src/app/Services/user-service.service";

@Component({
  selector: "app-user-posts",
  templateUrl: "./user-posts.component.html",
  styleUrls: ["./user-posts.component.scss"],
})
export class UserPostsComponent implements OnInit {
  posts: any[] = [];
  paginateValue = 1;
  constructor(private useServ: UserServiceService, private route: Router) {}

  ngOnInit() {
    this.getUserPosts(this.paginateValue);
  }

  getUserPosts(value) {
    this.useServ.getPosts(value).subscribe((res: any) => {
      this.posts = res;
      console.log("res of posts: ", res);
    });
  }

  paginateChange(value) {
    console.log("value changes: ", value);
    this.getUserPosts(value);
  }

  toDetails(postId) {
    this.route.navigate([`/post/${postId}`]);
  }
}
