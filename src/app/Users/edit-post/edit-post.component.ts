import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UserServiceService } from "src/app/Services/user-service.service";

@Component({
  selector: "app-edit-post",
  templateUrl: "./edit-post.component.html",
  styleUrls: ["./edit-post.component.scss"],
})
export class EditPostComponent implements OnInit {
  postId: any;
  post: any;
  constructor(
    private userServ: UserServiceService,
    private route: Router,
    private ActivatedRoute: ActivatedRoute
  ) {}

  //start .. edit item
  itemForm = new FormGroup({
    title: new FormControl("", [Validators.required, Validators.minLength(1)]),

    body: new FormControl("", [Validators.required, Validators.minLength(1)]),
  });

  get title() {
    return this.itemForm.get("title");
  }

  get body() {
    return this.itemForm.get("body");
  }
  // end .. edit item

  ngOnInit() {
    this.ActivatedRoute.params.subscribe((res: any) => {
      console.log("res of route: ", res);
      if (res.id) {
        this.postId = res.id;
        this.getPostdetails(res.id);
      }
    });
  }

  getPostdetails(id) {
    this.userServ.getPostDetail(id).subscribe(
      (res: any) => {
        this.post = res;
      },
      (err) => {
        console.log("error of getDetail: ", err);
      }
    );
  }

  editPost() {
    console.log("this.itemForm.value: ", this.itemForm.value);
    this.userServ.editPost(this.postId, this.itemForm.value).subscribe(
      (res: any) => {
        this.route.navigate(["/"]);
      },
      (err) => {
        console.log("error of add product:", err);
      }
    );
  }
}
