import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class UserServiceService {
  constructor(public http: HttpClient) {}
  getPosts(paginateValue) {
    let limit;
    if (paginateValue == 1) {
      limit = 5;
    }
    if (paginateValue == 2) {
      limit = 10;
    }
    if (paginateValue == 3) {
      limit = 15;
    }
    return this.http.get(
      `https://jsonplaceholder.typicode.com/posts?_start=0&_limit=${limit}`
    );
  }
  getPostDetail(id) {
    return this.http.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
  }

  getPostComments(postId) {
    return this.http.get(
      ` https://jsonplaceholder.typicode.com/comments?postId=${postId}`
    );
  }

  editPost(id, values) {
    return this.http.patch(
      `https://jsonplaceholder.typicode.com/posts/${id}`,
      values
    );
  }
}
